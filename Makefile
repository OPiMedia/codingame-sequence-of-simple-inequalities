# Static analysis and cleaning (for Python language) --- December 31, 2020
.SUFFIXES:

PROG_SRC = $(sort $(wildcard *.py))
SRC = $(PROG_SRC)


PYTHON      = python3  # https://www.python.org/
PYTHONFLAGS =

PYPY      = pypy3  # https://www.pypy.org/
PYPYFLAGS =


MYPY      = mypy  # http://www.mypy-lang.org/
MYPYFLAGS = # --warn-unused-ignores

PYCODESTYLE      = pycodestyle  # (pep8) https://pypi.org/project/pycodestyle/
PYCODESTYLEFLAGS = --statistics  # --ignore=E501

PYFLAKES      = pyflakes  # https://pypi.org/project/pyflakes/
PYFLAKESFLAGS =

PYLINT      = pylint  # https://www.pylint.org/
PYLINTFLAGS = -j $(JOB) --disable=line-too-long,locally-disabled,missing-class-docstring,missing-function-docstring,missing-module-docstring --good-names=a,b,c,d,e,f,g,h,i,j,k,l,m,n,nb,o,ok,p,q,r,s,t,u,v,w,x,x0,x1,y,y0,y1,z

PYRE      = pyre  # https://pyre-check.org/
PYREFLAGS = # --strict

PYTYPE      = pytype  # https://google.github.io/pytype/
PYTYPEFLAGS = -P $(PWD):$(PYTHONPATH) -k -j $(JOB)


CAT      = cat
DOS2UNIX = dos2unix
ECHO     = echo
GREP     = grep
HEAD     = head
READ     = read
RM       = rm -f
RMDIR    = rmdir
SHELL    = sh
TAIL     = tail
TEE      = tee
UNIX2DOS = unix2dos



JOB ?= 1  # change this by define new value when start: $ make JOB=3



###
# #
###
.PHONY: all dos2unix unix2dos

all:	lintlog

dos2unix:
	$(DOS2UNIX) data/correct/*.txt
	$(DOS2UNIX) data/input/*.txt

unix2dos:
	$(UNIX2DOS) data/correct/*.txt
	$(UNIX2DOS) data/input/*.txt



###################
# Static analysis #
###################
.PHONY: lint lintlog mypy pycodestyle pyflakes pylint pyre pytype

lint:	pycodestyle pyflakes pylint mypy pytype .pyre_configuration pyre

lintlog:
	@$(ECHO) 'Lint ('`date`') of solution.py' | $(TEE) lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pycodestyle '(pep8)' `$(PYCODESTYLE) $(PYCODESTYLEFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pyflakes `$(PYFLAKES) $(PYFLAKESFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== `$(PYLINT) $(PYLINTFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYLINT) $(PYLINTFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== `$(MYPY) $(MYPYFLAGS) --version` ===== | $(TEE) -a lint.log
	-export MYPYPATH=$(PWD):$(PYTHONPATH); $(MYPY) $(MYPYFLAGS) $(SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== pytype `$(PYTYPE) $(PYTYPEFLAGS) --version` ===== | $(TEE) -a lint.log
	-$(PYTYPE) $(PYTYPEFLAGS) $(PROG_SRC) 2>&1 | $(TEE) -a lint.log
	@$(ECHO) | $(TEE) -a lint.log
	@$(ECHO) ===== Pyre `$(PYRE) $(PYREFLAGS) --version | $(HEAD) -n 1` ===== | $(TEE) -a lint.log
	-$(PYRE) $(PYREFLAGS) --noninteractive check 2>&1 | $(GREP) -E -v '[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2},[0-9]+ \[PID [0-9]+\] [^ES]' | $(TEE) -a lint.log


mypy:
	@$(ECHO) ===== `$(MYPY) $(MYPYFLAGS) --version` =====
	-export MYPYPATH=$(PWD):$(PWD)/tests:$(PYTHONPATH); $(MYPY) $(MYPYFLAGS) $(SRC)
	@$(ECHO)

pycodestyle:
	@$(ECHO) ===== pycodestyle '(pep8)' `$(PYCODESTYLE) $(PYCODESTYLEFLAGS) --version` =====
	-$(PYCODESTYLE) $(PYCODESTYLEFLAGS) $(SRC)
	@$(ECHO)

pyflakes:
	@$(ECHO) ===== pyflakes `$(PYFLAKES) $(PYFLAKESFLAGS) --version` =====
	-$(PYFLAKES) $(PYFLAKESFLAGS) $(SRC)
	@$(ECHO)

pylint:
	@$(ECHO) ===== `$(PYLINT) $(PYLINTFLAGS) --version` =====
	-$(PYLINT) $(PYLINTFLAGS) -f colorized $(SRC)
	@$(ECHO)

pyre:	.pyre_configuration
	@$(ECHO) ===== Pyre `$(PYRE) $(PYREFLAGS) --version | $(HEAD) -n 1` =====
	-$(PYRE) $(PYREFLAGS)
	@$(ECHO)

.pyre_configuration:
	$(ECHO) '.' | $(PYRE) init
	$(TAIL) -n +2 $@ > $@_TMP
	$(ECHO) '{\n  "exclude": [".*\/data\/.*", ".*\/\\\\.mypy_cache\/.*", ".*\/\\\\.pytype\/.*"],' | $(CAT) - $@_TMP > $@
	$(RM) $@_TMP

pytype:
	@$(ECHO) ===== pytype `$(PYTYPE) $(PYTYPEFLAGS) --version` =====
	-$(PYTYPE) $(PYTYPEFLAGS) $(PROG_SRC)
	@$(ECHO)



#########
# Clean #
#########
.PHONY:	clean cleanData cleanLint cleanResult distclean forceCleanData keepFileEmptyDirectory overclean

clean:	cleanResult
	$(RM) -r .mypy_cache
	$(RM) .pyre_configuration
	$(RM) -r .pyre
	$(RM) -r .pytype
	if [ -z $(KEEPFILEEMPTYDIRECTORY) ]; then $(RM) data/correct/PUT_CORRECT_OUTPUT_FILES_HERE data/input/PUT_INPUT_FILES_HERE; fi

cleanData:
	if [ -z $(FORCECLEANDATA) ]; then $(READ) -p 'Delete "data" directory? [Y/n]' answer; if [ "$$answer" != 'Y' ]; then exit 1; fi; fi
	-$(RM) data/correct/*.txt
	-$(RM) data/input/*.txt

cleanLint:
	$(RM) lint.log

cleanResult:
	-$(RM) result/*.txt
	-$(RMDIR) result

distclean:	clean

forceCleanData:
	$(eval FORCECLEANDATA = true)

keepFileEmptyDirectory:
	$(eval KEEPFILEEMPTYDIRECTORY = true)

overclean:	distclean cleanLint cleanData
