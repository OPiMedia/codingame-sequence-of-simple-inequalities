#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Generate random data.
"""

import sys
import random


def main() -> None:
    n = (int(sys.argv[1]) if len(sys.argv) > 1
         else 10)

    print(n)
    random.seed()

    lower_bound = -123456
    upper_bound = 123456

    lower = random.randint(lower_bound, upper_bound)
    upper = random.randint(lower, upper_bound - 1)

    for _ in range(n):
        if random.choice((False, True)):
            value = random.randint(lower_bound, lower)
            operator = random.choice(('>', '>='))
        else:
            value = random.randint(upper, upper_bound)
            operator = random.choice(('<', '<='))
        print(f'x {operator} {value}')


if __name__ == '__main__':
    main()
