#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Sequence of simple inequalities

Clash of code for CodinGame:
https://www.codingame.com/contribute/view/607063f19d22b0acfe7a476fbce91513eaf4
"""

import sys


# One solution for minimum size of code (probably not optimal)
"""
I=input
u=9**9
l=-u
for _ in' '*int(I()):
 _,o,s=I().split();v=int(s)
 if o in('<','<='):
  if o =='<':v-=1
  u=min(u,v)
 else:
  if o =='>':v+=1
  l=max(l,v)
I(f'{l} <= x <= {u}'if l<=u else'no solution')

exit(0)
"""


def main() -> None:
    nb = int(input())

    assert 2 <= nb <= 100

    lower_bound = -123456
    upper_bound = 123456

    lower = lower_bound
    upper = upper_bound

    found_smaller = False
    found_greater = False

    for _ in range(nb):
        line = input()
        variable, operator, value_str = line.split()
        value = int(value_str)

        assert variable == 'x'
        assert operator in ('<', '<=', '>', '>=')
        assert lower_bound <= value <= upper_bound

        if operator in ('<', '<='):
            if operator == '<':
                value -= 1
            upper = min(upper, value)
            found_smaller = True
        else:  # > or >=
            if operator == '>':
                value += 1
            lower = max(lower, value)
            found_greater = True

    print(f'{lower} <= x <= {upper}' if lower <= upper
          else 'no solution')

    assert lower_bound <= lower <= upper_bound
    assert lower_bound <= upper <= upper_bound
    assert found_smaller and found_greater
    assert sys.stdin.readline() == ''


if __name__ == '__main__':
    main()
