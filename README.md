# Sequence of simple inequalities (CodinGame, January 9, 2021)

Clash of code for CodinGame:
https://www.codingame.com/contribute/view/607063f19d22b0acfe7a476fbce91513eaf4



## Statement
```
Found the interval of possible values for an integer variable x from a sequence of simple inequalities like this:
"x < 124",
"x <= 123",
"x > -667" or
"x >= -666"

Print the solution interval like this:
"-666 <= x <= 123"

If there is no possible value for x then print:
"no solution"
```



## Inputs
```
<<Line 1:>> Positive integer [[nb]].

<<Next [[nb]] lines:>> string of the form "x [[operator]] [[value]]" where each [[operator]] is "<", "<=", ">" or ">=" and each [[value]] is an integer.

In each test case there is garantee that there is at least one < or <= and one > or >= operators.
```



## Outputs
```
<<Line 1:>> "[[a]] <= x <= [[b]]" with correct values for [[a]] and [[b]], or "no solution".
```



## Constraints
```
2 ≤ [[nb]] ≤ 100
-123456 ≤ [[value]] ≤ 123456
```



## Sub generator
```
read nb:int
loop nb read s:string(100)

write answer
```



## Link
* [**HackerRank [CodinGame…] / helpers**](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  little scripts to help in solving problems of HackerRank website (or CodinGame…)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬

🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png
